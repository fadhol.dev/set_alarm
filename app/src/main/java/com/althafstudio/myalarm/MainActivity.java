package com.althafstudio.myalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    PendingIntent pendingIntent;
    AlarmManager alarmManager;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startAlertAtParticularTime();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void startAlertAtParticularTime() {

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent myIntent = new Intent(MainActivity.this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent, 0);

        Calendar calendar = Calendar.getInstance();
        //calendar.set(Calendar.YEAR, 2018);
        //calendar.set(Calendar.MONTH, 3);
        //calendar.set(Calendar.DATE, 24);
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 51);

        long currentTime = calendar.getTimeInMillis();
        //long fireTime = 10 * 60 * 1000;
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, currentTime, pendingIntent);

        Toast.makeText(this, "Alarm will vibrate at time specified",
                Toast.LENGTH_SHORT).show();

    }
}
